﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;

namespace Sql
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: sql <driver assembly> <driver type> <connection string> <command>");
                return;
            }
            Assembly drvasm;
            if (File.Exists(args[0]))
                drvasm = Assembly.LoadFile(Path.GetFullPath(args[0]));
            else
                drvasm = Assembly.Load(args[0]);
            var drvtype=drvasm.GetType(args[1]);
            var drvinstance = (IDbConnection) Activator.CreateInstance(drvtype);
            drvinstance.ConnectionString = args[2];
            drvinstance.Open();
            var cmd=drvinstance.CreateCommand();
            cmd.CommandText = args[3];
            Console.WriteLine(cmd.ExecuteScalar());
        }
    }
}
