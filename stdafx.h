#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <Pdh.h>
#include <PdhMsg.h>
#include <WbemCli.h> 
#include <strsafe.h>
#include <snmp.h>
#include <malloc.h>
#include <io.h>
#include <fcntl.h>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <ios>
#include <cctype>
#include <sstream>
#include <fstream>
#include <locale>
#include <codecvt>

#define LOGGING 1

#if LOGGING
extern std::ofstream logger;
#endif

std::string cvt(const std::wstring &src);
std::wstring cvt(const std::string &src);
