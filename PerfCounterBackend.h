#pragma once
#include "ValueBackend.h"
#include "stdafx.h"

class CounterData;
typedef std::map<PDH_HCOUNTER, CounterData> CounterDataMap;
class PerfCounterBackend :public AbstractValueBackend{
private:
	PDH_HQUERY query;
	CounterDataMap counterData;
	HANDLE collectionThread;
	HANDLE stopper;
public:
	PerfCounterBackend();
	~PerfCounterBackend();
	void Start();
	DWORD ThreadProc();
	void *Prepare(const ConfigElement &el);
	int GetInt(const ConfigElement &el);
	std::wstring GetString(const ConfigElement &el);
};
