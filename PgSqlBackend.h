#pragma once

#include "stdafx.h"
#include "ValueBackend.h"

class PgSqlBackend :public AbstractValueBackend{
private:
	void* connection;
public:
	PgSqlBackend();
	~PgSqlBackend();
	void* Prepare(const ConfigElement &el);
	int GetInt(const ConfigElement &el);
	std::wstring GetString(const ConfigElement &el);
};