#pragma once
#include "ValueBackend.h"
#include "stdafx.h"

class WMIBackend :public AbstractValueBackend{
private:
	IWbemLocator *locator;
	std::map<std::wstring, IWbemServices*> services;
	VARIANT Execute(const ConfigElement &el);
public:
	WMIBackend();
	~WMIBackend();
	void* Prepare(const ConfigElement &el);
	int GetInt(const ConfigElement &el);
	std::wstring GetString(const ConfigElement &el);
};