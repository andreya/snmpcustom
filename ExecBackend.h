#pragma once
#include "stdafx.h"
#include "ValueBackend.h"

struct ProcInfo;
class ExecBackend :public AbstractValueBackend{
private:
	ProcInfo Execute(const ConfigElement& el);
public:
	int GetInt(const ConfigElement &el);
	std::wstring GetString(const ConfigElement &el);
};