#include "stdafx.h"
#include "PgSqlBackend.h"
#include "libpq-fe.h"
//#include "catalog/pg_type.h"
std::map<std::string, PGconn*> connectionPool;
PgSqlBackend::PgSqlBackend(){

}
PgSqlBackend::~PgSqlBackend(){

}
void* PgSqlBackend::Prepare(const ConfigElement &el){
	LPSTR msg;
	std::string cstr = cvt(el.ConnectionString);
	LPCSTR connStr = cstr.c_str();
	PQconninfoOption *opts=PQconninfoParse(connStr, &msg);
	char *host=NULL, *db=NULL;
	if (msg){
#if LOGGING
		logger << "PgSqlBackend::Prepare cannot parse connection string: " << msg << std::endl << std::flush;
#endif
		PQfreemem(msg);
		return NULL;
	}else{
		PQconninfoOption *tmp = opts;
		while (tmp->keyword != NULL){
			if (lstrcmpA(tmp->keyword, "host")==0)
				host = tmp->val;
			else if (lstrcmpA(tmp->keyword, "dbname")==0)
				db = tmp->val;
			tmp++;
		}
		if (host == NULL)
			host = "127.0.0.1";
		if (db == NULL)
			db = "template0";
		std::string key(host);
		key += db;

		PQconninfoFree(opts);

		std::map<std::string, PGconn*>::iterator iter = connectionPool.find(key);
		if (iter == connectionPool.end()){
			PGconn* cnn = PQconnectdb(connStr);
			if (cnn != NULL && PQstatus(cnn)==CONNECTION_OK){
				connectionPool.insert(std::pair<std::string, PGconn*>(key, cnn));
				return cnn;
			}
			else{
#if LOGGING
				logger << "PgSqlBackend::Prepare cannot connect to database: " << PQerrorMessage(cnn) << std::endl << std::flush;
#endif
			}
			return NULL;
		}
		return iter->second;
	}
}

std::pair<Oid,void*> Execute(const ConfigElement &el){
	std::pair<Oid, void*> rv;
	PGconn *cnn=(PGconn*)el.BackendData;
	if (PQstatus(cnn) == CONNECTION_BAD){
#if LOGGING
		logger << "PgSqlBackend::Execute resetting connection." << std::endl << std::flush;
#endif
		PQreset(cnn);
	}
	PGresult *result=PQexec(cnn, cvt(el.Param).c_str());
	ExecStatusType status = PQresultStatus(result);
	if (status == PGRES_TUPLES_OK || status == PGRES_SINGLE_TUPLE){
		if (PQnfields(result) == 1 && PQntuples(result) > 0){
			Oid type = PQftype(result, 0);
			int len = PQgetlength(result, 0, 0);
			char* buf = (char*)LocalAlloc(LMEM_FIXED, len+1);
			buf[len] = 0;
			CopyMemory(buf, PQgetvalue(result, 0, 0), len);
			rv = std::pair<Oid, void*>(type, buf);
		}
		else{
#if LOGGING
			logger << "PgSqlBackend::Execute query execution failed: result is empty or contains more than one column." << std::endl << std::flush;
#endif
		}
	}
	else{
#if LOGGING
		logger << "PgSqlBackend::Execute cannot execute query: " << PQerrorMessage(cnn) << std::endl << std::flush;
#endif
	}
	PQclear(result);
	return rv;
}

int PgSqlBackend::GetInt(const ConfigElement &el){
	std::pair<Oid, void*> data=Execute(el);	
	int rv = data.second == NULL ? 0 : atoi((char*)data.second);
	LocalFree(data.second);
	return rv;
}
std::wstring PgSqlBackend::GetString(const ConfigElement &el){
	std::pair<Oid, void*> data = Execute(el);
	std::string str((char*)data.second);
	LocalFree(data.second);
	return cvt(str);
}
