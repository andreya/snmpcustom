﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Configurator.Annotations;
using Microsoft.Win32;

namespace Configurator
{
    public class Config:INotifyPropertyChanged
    {
        private string _prefix;
        private ObservableCollection<ConfigElement> _elements;

        public Config()
        {
            Elements = new ObservableCollection<ConfigElement>();
        }

        public void Read()
        {
            using (var key = Registry.LocalMachine.OpenSubKey("Software\\SNMPCustom", false))
            {
                Prefix = (string) key.GetValue("Prefix", "");
                Elements.Clear();
                foreach (var subkey in key.GetSubKeyNames())
                {
                    var element = new ConfigElement();
                    element.Oid = subkey;
                    using (var subhkey = key.OpenSubKey(subkey, false))
                    {
                        BackendType t;
                        if (!Enum.TryParse(subhkey.GetValue("Backend", "").ToString(), true, out t))
                            t = BackendType.Exec;

                        element.Backend = t;
                        if (t == BackendType.Exec)
                        {
                            ExecMode tt;
                            if (!Enum.TryParse(subhkey.GetValue("Mode", "").ToString(), true, out tt))
                                tt = ExecMode.Out;
                            element.ExecMode = tt;
                        }
                        else if (t == BackendType.PerfCounter)
                        {
                            AveragingMode tt;
                            if (!Enum.TryParse(subhkey.GetValue("Mode", "").ToString(), true, out tt))
                                tt = AveragingMode.Sample;
                            element.AveragingMode = tt;
                        }
                        else if (t == BackendType.PgSQL)
                        {
                            element.ConnString = (subhkey.GetValue("ConnectionString") ?? "").ToString();
                        }
                        Type ttt;
                        if (!Enum.TryParse(subhkey.GetValue("Type", "").ToString(), true, out ttt))
                            ttt = Type.Int;
                        element.Type = ttt;

                        element.Param = subhkey.GetValue("Param", "").ToString();
                        Elements.Add(element);
                    }
                }
            }
        }

        public void Write()
        {
            using (var key = Registry.LocalMachine.OpenSubKey("Software\\SNMPCustom", true))
            {
                key.SetValue("Prefix", Prefix);
                foreach (var subkey in key.GetSubKeyNames())
                {
                    key.DeleteSubKey(subkey);
                }
                foreach (var element in Elements)
                {
                    using (var subkey = key.CreateSubKey(element.Oid))
                    {
                        subkey.SetValue("Type", element.Type.ToString());
                        subkey.SetValue("Param", element.Param);
                        subkey.SetValue("Backend", element.Backend);
                        if (element.Backend == BackendType.Exec)
                            subkey.SetValue("Mode", element.ExecMode.ToString());
                        else if (element.Backend == BackendType.PerfCounter)
                            subkey.SetValue("Mode", element.AveragingMode.ToString());
                        else if (element.Backend == BackendType.PgSQL)
                            subkey.SetValue("ConnectionString", element.ConnString);
                    }
                }
            }
        }

        public string Prefix
        {
            get { return _prefix; }
            set
            {
                if (value == _prefix) return;
                _prefix = value;
                OnPropertyChanged("Prefix");
            }
        }

        public ObservableCollection<ConfigElement> Elements
        {
            get { return _elements; }
            set
            {
                if (Equals(value, _elements)) return;
                _elements = value;
                OnPropertyChanged("Elements");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
