﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Path = System.IO.Path;

namespace Configurator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var model = new Config();
            model.Read();
            DataContext = model;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            ((Config)DataContext).Write();
            try
            {
                var sc = new ServiceController("SNMP");
                if (sc.Status == ServiceControllerStatus.Running)
                {
                    if (MessageBox.Show(this, "Settings saved. Do you want to restart SNMP service to apply new configuration?", "Information", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                    {
                        Cursor = Cursors.Wait;
                        ThreadPool.QueueUserWorkItem(o =>
                        {
                            sc.Stop();
                            sc.WaitForStatus(ServiceControllerStatus.Stopped);
                            sc.Start();
                            Dispatcher.Invoke(new Action(() =>
                            {
                                Cursor = Cursors.Arrow;
                            }));
                        });
                    }
                }
            }
            catch
            {
                MessageBox.Show(this, "Cannot communicate with SNMP service, mybe it is not installed? Although your settings was saved successfully.");
            }
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddSensorClick(object sender, RoutedEventArgs e)
        {
            ((Config)DataContext).Elements.Add(new ConfigElement());
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            ((Config)DataContext).Elements.Remove((ConfigElement)((Control)e.OriginalSource).DataContext);
        }

        private void WMISelectClick(object sender, RoutedEventArgs e)
        {
            
        }

        private void PerfCounterSelectClick(object sender, RoutedEventArgs e)
        {
            var dlg = new SelectPerformanceCounter(((ConfigElement) ((Control) e.OriginalSource).DataContext).Param);
            if (dlg.ShowDialog() ?? false)
            {
                ((ConfigElement) ((Control) e.OriginalSource).DataContext).Param = dlg.SelectedItem;
            }
        }

        private void InstallClick(object sender, RoutedEventArgs e)
        {
            RegistryKey key;
            bool installed = true;
            using (key = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\SNMP\\Parameters\\ExtensionAgents", true))
            {
                if (key == null)
                {
                    MessageBox.Show(this, "Cannot find SNMP service registry key. Probably you forgot to install it in the first place?", "Error");
                    return;
                }
                var path = key.GetValue("SNMPCustom");
                installed = (path != null && path.ToString() == "Software\\SNMPCustom");
                if (!installed)
                    key.SetValue("SNMPCustom", "Software\\SNMPCustom");
            }
            using (key = Registry.LocalMachine.CreateSubKey("Software\\SNMPCustom"))
            {
                var path = key.GetValue("Pathname") as string;
                if (path != null && File.Exists(path))
                {
                    if (installed)
                    {
                        MessageBox.Show(this, "SNMP extension already installed.", "Success");
                    }
                    else
                    {
                        MessageBox.Show(this, "SNMP extension was already installed but disabled. Extension enabled.", "Success");
                    }
                    return;
                }
                path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "SNMPCustom.dll");
                if (!File.Exists(path))
                {
                    var ofd = new OpenFileDialog();
                    ofd.Filter = "SNMPCustom.dll|SNMPCustom.dll|Any DLL file|*.dll|Any file|*.*";
                    ofd.Title = "Locate extension DLL";
                    if (ofd.ShowDialog(this) ?? false)
                    {
                        path = ofd.FileName;
                    }
                    else
                    {
                        MessageBox.Show(this, "Cannot install extension as it was not found.", "Error");
                        return;
                    }
                }
                key.SetValue("Pathname", path);
                MessageBox.Show(this, "Extension installed. Remember to set proper permissions on SNMP service in service manager!", "Success");
            }
        }
    }
}
