﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Configurator
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException+=OnException;
            try
            {
                Configurator.App app = new Configurator.App();
                app.InitializeComponent();
                app.Run();
            }
            catch (Exception e)
            {
                OnException(null,new UnhandledExceptionEventArgs(e,true));
            }
            return 0;
        }

        private static void OnException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e);
            using (var fs = new StreamWriter(Path.Combine(Path.GetTempPath(), "Configurator.exception")))
            {
                fs.WriteLine(e);
            }
        }
    }
}
