namespace Configurator
{
    public enum BackendType
    {
        PerfCounter,
        Exec,
        WMI,
        PgSQL
    }
}