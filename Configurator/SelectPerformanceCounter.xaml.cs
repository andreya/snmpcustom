﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Configurator
{
    /// <summary>
    /// Interaction logic for SelectPerformanceCounter.xaml
    /// </summary>
    public partial class SelectPerformanceCounter : Window
    {
        private class PCCategory : PCItem
        {
            public PCItem[] Elements { get; set; }
        }

        private class PCItem
        {
            public string Name { get; set; }
            public bool Selected { get; set; }
        }

        private class SelectPCModel
        {
            private PCItem[] Counters(IEnumerable<PerformanceCounter> src, string item)
            {
                return src.OrderBy(z => z.CounterName)
                    .Select(z => new PCItem
                    {
                        Name = z.CounterName,
                        Selected = z.CounterName == item
                    }).ToArray();
            }

            public SelectPCModel(string preselect)
            {
                var items = preselect.Trim('\\').Split(new[] {'\\'}, 2);
                var category = items[0];
                var item = items.Length > 1 ? items[1] : null;
                string instance = null;
                var index = category.IndexOf('(');
                if (index != -1)
                {
                    instance = category.Substring(index + 1, category.Length - index - 2);
                    category = category.Substring(0, index).Trim();
                }
                if (CachedTree == null)
                {
                    CachedTree = PerformanceCounterCategory.GetCategories().OrderBy(x => x.CategoryName).Select(x => new PCCategory
                    {
                        Name = x.CategoryName,
                        Selected = x.CategoryName == category,
                        Elements = x.CategoryType == PerformanceCounterCategoryType.SingleInstance
                            ? Counters(x.GetCounters(), item)
                            : x.GetInstanceNames().Select(i =>
                            {
                                try
                                {
                                    var ctrs = x.GetCounters(i);
                                    return (PCItem) new PCCategory
                                    {
                                        Name = i,
                                        Selected = i == instance,
                                        Elements = Counters(ctrs, item)
                                    };
                                }
                                catch
                                {
                                    return null;
                                }
                            }
                                ).Where(i => i != null).ToArray()
                    }).ToArray();
                }
                else
                {
                    foreach (var pcCategory in CachedTree)
                    {
                        pcCategory.Selected = pcCategory.Name == category;
                        foreach (var element in pcCategory.Elements)
                        {
                            if (element is PCCategory)
                            {
                                element.Selected = element.Name == instance;
                                foreach (var pcItem in ((PCCategory) element).Elements)
                                {
                                    pcItem.Selected = pcItem.Name == item;
                                }
                            }
                            else
                            {
                                element.Selected = element.Name == item;
                            }
                        }
                    }
                }
                Categories = CachedTree;
            }

            private static PCCategory[] CachedTree;
            public PCCategory[] Categories { get; set; }
        }



        public SelectPerformanceCounter(string preselect)
        {
            InitializeComponent();
            DataContext = new SelectPCModel(preselect ?? "");
        }

        private void PCTree_OnSelected(object sender, RoutedEventArgs e)
        {
            var element = e.OriginalSource as FrameworkElement;
            if (element != null)
            {
                var relativePosition = element.TranslatePoint(new Point(0, 0), Scroller);
                element.BringIntoView();
                if(Scroller.VerticalOffset<relativePosition.Y||Scroller.VerticalOffset+Scroller.Height>relativePosition.Y)
                    Scroller.ScrollToVerticalOffset(relativePosition.Y);

                var item = element.DataContext as PCItem;
                
                var items = new Stack<PCItem>();
                if (!(item is PCCategory))
                {
                    while (element != null&&!(element is TreeView))
                    {
                        element = (FrameworkElement) VisualTreeHelper.GetParent(element);
                        if(element is TreeViewItem)
                            items.Push((PCItem)element.DataContext);
                    }

                    if (items.Count == 2)
                        SelectedItem = string.Format("{0}({1})\\{2}", items.Pop().Name, items.Pop().Name, item.Name);
                    else
                        SelectedItem = string.Format("{0}\\{1}", items.Pop().Name, item.Name);
                }
            }
        }

        public string SelectedItem { get; private set; }
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
