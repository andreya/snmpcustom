using System.ComponentModel;
using System.Management.Instrumentation;
using System.Windows.Media.Animation;
using Configurator.Annotations;

namespace Configurator
{
    public class ConfigElement:INotifyPropertyChanged
    {
        private BackendType _backend;
        private string _param;
        private bool _execModeAvailable;
        private bool _wmiAvailable;
        private ExecMode _execMode;
        private bool _averagingModeAvailable;
        private AveragingMode _averagingMode;
        private Type _type;
        private string _oid;
        private string _connString;
        private bool _pgsqlAvailable;

        public ConfigElement()
        {
            Backend=BackendType.PerfCounter;
        }

        public string Oid
        {
            get { return _oid; }
            set
            {
                if (value == _oid) return;
                _oid = value;
                OnPropertyChanged("Oid");
            }
        }

        public BackendType Backend
        {
            get { return _backend; }
            set
            {
                //if (value == _backend) return;
                _backend = value;
                OnPropertyChanged("Backend");
                ExecModeAvailable = value == BackendType.Exec;
                AveragingModeAvailable = value == BackendType.PerfCounter;
                WMIAvailable = value == BackendType.WMI;
                PgSQLAvailable = value == BackendType.PgSQL;
            }
        }

        public string Param
        {
            get { return _param; }
            set
            {
                if (value == _param) return;
                _param = value;
                OnPropertyChanged("Param");
            }
        }

        public string ConnString
        {
            get { return _connString; }
            set
            {
                if (value == _connString) return;
                _connString = value;
                OnPropertyChanged("ConnString");
            }
        }
        public bool PgSQLAvailable
        {
            get { return _pgsqlAvailable; }
            set
            {
                if (value.Equals(_pgsqlAvailable)) return;
                _pgsqlAvailable = value;
                OnPropertyChanged("PgSQLAvailable");
            }
        }
        public bool WMIAvailable
        {
            get { return _wmiAvailable; }
            set
            {
                if (value.Equals(_wmiAvailable)) return;
                _wmiAvailable = value;
                OnPropertyChanged("WMIAvailable");
            }
        }
        public bool ExecModeAvailable
        {
            get { return _execModeAvailable; }
            set
            {
                if (value.Equals(_execModeAvailable)) return;
                _execModeAvailable = value;
                OnPropertyChanged("ExecModeAvailable");
            }
        }

        public ExecMode ExecMode
        {
            get { return _execMode; }
            set
            {
                if (value == _execMode) return;
                _execMode = value;
                OnPropertyChanged("ExecMode");
            }
        }

        public bool AveragingModeAvailable
        {
            get { return _averagingModeAvailable; }
            set
            {
                if (value.Equals(_averagingModeAvailable)) return;
                _averagingModeAvailable = value;
                OnPropertyChanged("AveragingModeAvailable");
            }
        }

        public AveragingMode AveragingMode
        {
            get { return _averagingMode; }
            set
            {
                if (value == _averagingMode) return;
                _averagingMode = value;
                OnPropertyChanged("AveragingMode");
            }
        }

        public Type Type
        {
            get { return _type; }
            set
            {
                if (value == _type) return;
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        public Type[] TypeItems
        {
            get
            {
                return new[]
                {
                    Type.Int,
                    Type.String
                };
            }
        }

        public AveragingMode[] AveragingItems
        {
            get
            {
                return new[]
                {
                    AveragingMode.Average,
                    AveragingMode.Min,
                    AveragingMode.Max,
                    AveragingMode.Sample
                };
            }
        }
        public ExecMode[] ExecItems
        {
            get
            {
                return new[]
                {
                    ExecMode.Out,
                    ExecMode.Err,
                    ExecMode.RetVal
                };
            }
        }
        public BackendType[] BackendItems
        {
            get
            {
                return new[]
                {
                    BackendType.Exec,
                    BackendType.PerfCounter,
                    BackendType.WMI,
                    BackendType.PgSQL,
                };
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}