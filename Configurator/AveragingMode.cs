namespace Configurator
{
    public enum AveragingMode
    {
        Average,
        Min,
        Max,
        Sample
    }
}