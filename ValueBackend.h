#pragma once
#include "stdafx.h"
#include "ConfigBackend.h"

class AbstractValueBackend{
public:
	virtual void* Prepare(const ConfigElement &el){ return NULL; }
	virtual void Start(){}
	virtual int GetInt(const ConfigElement &el) = 0;
	virtual std::wstring GetString(const ConfigElement &el) = 0;
};

