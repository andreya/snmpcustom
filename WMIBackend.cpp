#include "stdafx.h"
#include "WMIBackend.h"

WMIBackend::WMIBackend(){
	HRESULT hr;
	if (FAILED(hr = CoInitializeEx(NULL, COINIT_MULTITHREADED))){
#if LOGGING
		logger << "WMIBackend::WMIBackend CoInitializeEx failed" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}
	if (FAILED(hr = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, 0))){
#if LOGGING
		logger << "WMIBackend::WMIBackend CoInitializeSecurity failed" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}	
	if (FAILED(hr = CoCreateInstance(CLSID_WbemLocator, NULL, CLSCTX_ALL, IID_PPV_ARGS(&locator)))){
#if LOGGING
		logger << "WMIBackend::WMIBackend cannot acquire IWbemLocator" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}

}
WMIBackend::~WMIBackend(){
	for (std::map<std::wstring, IWbemServices*>::iterator iter = services.begin(); iter != services.end(); iter++){
		iter->second->Release();
	}
	locator->Release();
}
void* WMIBackend::Prepare(const ConfigElement &el) {
	int colon = el.Param.find(':');
	std::wstring ns(TEXT("root\\CIMV2"));
	if (colon != -1)
		ns = el.Param.substr(0, colon);

	std::map<std::wstring, IWbemServices*>::iterator iter = services.find(ns);
	if (iter == services.end())
	{
		IWbemServices *svc;

		BSTR str = SysAllocString(ns.c_str());
		if (FAILED(locator->ConnectServer(str, NULL, NULL, NULL, WBEM_FLAG_CONNECT_USE_MAX_WAIT, NULL, NULL, &svc))){
			SysFreeString(str);
#if LOGGING
			logger << "WMIBackend::Prepare cannot connect to " << cvt(ns) << std::endl << std::flush;
#endif
			throw E_CANNOT_INIT;
		}
		SysFreeString(str);
		services.insert(std::pair<std::wstring, IWbemServices*>(ns, svc));
		return svc;
	}
	else
		return iter->second;
}

VARIANT WMIBackend::Execute(const ConfigElement &el) {
	int colon = el.Param.find(':');
	std::wstring cmd(el.Param);
	if (colon != -1)
		cmd = el.Param.substr(colon + 1);

	IWbemServices* svc = ((IWbemServices*)el.BackendData);
	BSTR str = SysAllocString(cmd.c_str());
	IEnumWbemClassObject *enumerator;
	if (FAILED(svc->ExecQuery(TEXT("WQL"), str, WBEM_FLAG_FORWARD_ONLY, NULL, &enumerator))){
#if LOGGING
		logger << "WMIBackend::Execute execute query " << cvt(cmd) << std::endl << std::flush;
#endif
		SysFreeString(str);
		throw E_CANNOT_GET;
	}
	IWbemClassObject *obj;
	DWORD elems;
	HRESULT hr = enumerator->Next(WBEM_INFINITE, 1, &obj, &elems);
	VARIANT rv;
	VariantInit(&rv);
	if (!FAILED(hr)){
		if (hr != WBEM_S_FALSE){
			obj->BeginEnumeration(WBEM_FLAG_NONSYSTEM_ONLY);
			BSTR n;
			VARIANT tmp;
			if ((hr = obj->Next(0, &n, &tmp, NULL, NULL)) != WBEM_S_NO_MORE_DATA&&!FAILED(hr)){
				SysFreeString(n);
				VariantCopy(&rv, &tmp);
				VariantClear(&tmp);
			}
			obj->EndEnumeration();
		}
		obj->Release();
	}
	enumerator->Release();
	SysFreeString(str);
	return rv;
}

int WMIBackend::GetInt(const ConfigElement &el) {
	VARIANT rv = Execute(el);
	int rrv = 0;
	if (rv.vt == VT_I4)
		rrv = rv.intVal;
	VariantClear(&rv);
	return rrv;
}
std::wstring WMIBackend::GetString(const ConfigElement &el) {
	VARIANT rv = Execute(el);
	std::wstring rrv;
	if (rv.vt == VT_BSTR)
		rrv = rv.bstrVal;
	VariantClear(&rv);
	return rrv;
}