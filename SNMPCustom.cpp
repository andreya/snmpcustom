// SNMPCustom.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ConfigBackend.h"
#include "ValueBackend.h"
#include "WMIBackend.h"
#include "PerfCounterBackend.h"
#include "ExecBackend.h"
#include "PgSqlBackend.h"

Configuration gConfig;
std::vector<AbstractValueBackend*> gValueBackends;

BOOL SNMP_FUNC_TYPE SnmpExtensionInit(DWORD dwUptimeReference, HANDLE *phSubagentTrapEvent, AsnObjectIdentifier *pFirstSupportedRegion)
{
#if LOGGING
	logger << "::SnmpExtensionInit" << std::endl << std::flush;
#endif
	AbstractConfiguration* cfgBackend = new RegistryConfiguration();
	gValueBackends.push_back(new WMIBackend());
	gValueBackends.push_back(new PerfCounterBackend());
	gValueBackends.push_back(new ExecBackend());
	gValueBackends.push_back(new PgSqlBackend());
	try{
		gConfig = cfgBackend->ReadConfiguration();
		delete cfgBackend;
	}
	catch(int e) {
#if LOGGING
		logger << "::SnmpExtensionInit cannot load configuration: " << e << std::endl << std::flush;
#endif
		delete cfgBackend;
		return SNMPAPI_ERROR;
	}
	TCHAR tempName[256];
	for (std::vector<ConfigElement>::iterator it = gConfig.Items.begin(); it != gConfig.Items.end(); it++){
		SnmpUtilOidCpy(&(it->FullOid), &(gConfig.Prefix));
		SnmpUtilOidAppend(&(it->FullOid), &(it->Oid));
		LPSTR tmp=SnmpUtilOidToA(&(it->FullOid));
		
		MultiByteToWideChar(1252, MB_PRECOMPOSED, tmp, -1, tempName, 256);
		it->OidString = tempName;
		it->BackendData=gValueBackends[it->Backend]->Prepare(*it);
	}
	std::sort(gConfig.Items.begin(), gConfig.Items.end());
	*pFirstSupportedRegion =	gConfig.Prefix;
	for (int i = 0; i < 3; i++)
		gValueBackends[i]->Start();
#if LOGGING
	logger << "::SnmpExtensionInit finished" << std::endl << std::flush;
#endif
	return SNMPAPI_NOERROR;
}

ConfigElement* FindElement(SnmpVarBind *binding, bool next){
#if LOGGING
	logger << "::FindElement invoked name=" << SnmpUtilOidToA(&binding->name) << ", next=" << next << std::endl << std::flush;
#endif
	for (std::vector<ConfigElement>::iterator it = gConfig.Items.begin(); it != gConfig.Items.end(); it++){
		int cmp = SnmpUtilOidCmp(&(it->FullOid), &(binding->name));
		if (cmp<0)continue;
		if (next){
			if (cmp == 0){
				if (++it == gConfig.Items.end())
					return NULL;
			}
			SnmpUtilOidFree(&binding->name);
			SnmpUtilOidCpy(&binding->name, &(it->FullOid));			
		}
#if LOGGING
		logger << "::FindElement result name=" << SnmpUtilOidToA(&binding->name) << ", rv=" << cvt(it->OidString) << std::endl << std::flush;
#endif
		return &(*it);
	}
	return NULL;
}
int FillValue(SnmpVarBind *binding, ConfigElement* element){	
	std::wstring str;
	switch (binding->value.asnType = element->Type){
	case ASN_INTEGER:
		binding->value.asnValue.number = gValueBackends[element->Backend]->GetInt(*element); 
		break;
	case ASN_OCTETSTRING:
		str = gValueBackends[element->Backend]->GetString(*element);
		binding->value.asnValue.string.length = str.length()*sizeof(TCHAR);
		binding->value.asnValue.string.stream = (unsigned char*)SnmpUtilMemAlloc(binding->value.asnValue.string.length * sizeof(TCHAR));
		
		memcpy(binding->value.asnValue.string.stream, (LPSTR*)str.c_str(), binding->value.asnValue.string.length*sizeof(TCHAR));
		binding->value.asnValue.string.dynamic = TRUE;
		break;
	default:
		return SNMP_ERRORSTATUS_GENERR;
	}	
	return SNMP_ERRORSTATUS_NOERROR;
}

BOOL SNMP_FUNC_TYPE SnmpExtensionQuery(BYTE bPduType, SnmpVarBindList *pVarBindList, AsnInteger32 *pErrorStatus, AsnInteger32 *pErrorIndex)
{
	ConfigElement *element;
		for (UINT i = 0; i<pVarBindList->len; i++)
	{
		*pErrorStatus = SNMP_ERRORSTATUS_NOERROR;
		switch (bPduType)
		{
		case SNMP_PDU_GET:
			element = FindElement(&pVarBindList->list[i],false);
			if (element == NULL)
				*pErrorStatus = SNMP_ERRORSTATUS_NOSUCHNAME;
			else
				*pErrorStatus = FillValue(&pVarBindList->list[i], element);
			if (*pErrorStatus != SNMP_ERRORSTATUS_NOERROR)
				*pErrorIndex++;
			break;
		case SNMP_PDU_GETNEXT: // gets the next variable related to the passed variable in pVarBindList
			element = FindElement(&pVarBindList->list[i], true);
			if (element == NULL)
				*pErrorStatus = SNMP_ERRORSTATUS_NOSUCHNAME;
			else
				*pErrorStatus = FillValue(&pVarBindList->list[i], element);
			if (*pErrorStatus != SNMP_ERRORSTATUS_NOERROR)
				*pErrorIndex++;
			break;
		case SNMP_PDU_SET: 
			*pErrorStatus = SNMP_ERRORSTATUS_NOACCESS;
			*pErrorIndex++;
			break;
		default:
			*pErrorStatus = SNMP_ERRORSTATUS_NOSUCHNAME;
			*pErrorIndex++;
		};
	}

	return SNMPAPI_NOERROR;
}



BOOL SNMP_FUNC_TYPE SnmpExtensionTrap(AsnObjectIdentifier *pEnterpriseOid, AsnInteger32 *pGenericTrapId, AsnInteger32 *pSpecificTrapId, AsnTimeticks *pTimeStamp, SnmpVarBindList *pVarBindList)
{	
	return FALSE;
}
