#include "stdafx.h"
#include "ConfigBackend.h"

AsnObjectIdentifier ParseId(const std::wstring &str){
	std::wstring::const_iterator groupStart = str.begin();
	std::vector<UINT> parts;
	UINT current = 0;
	for (std::wstring::const_iterator it = str.begin(); it != str.end(); it++){
		wchar_t cur = *it;
		if (cur == TEXT('.')){
			if (it == groupStart){
#if LOGGING
				logger << "::ParseId cannot parse OID " << cvt(str) << " (empty group)" << std::endl << std::flush;
#endif
				throw E_OID_ERROR;
			}
			parts.push_back(current);
			current = 0;
			groupStart = it;
		}
		else if (cur >= TEXT('0') && cur <= TEXT('9'))
		{
			current *= 10;
			current += cur - TEXT('0');
		}
		else{
#if LOGGING
			logger << "::ParseId cannot parse OID " << cvt(str) << " (invalid character)" << std::endl << std::flush;
#endif
			throw E_OID_ERROR;
		}
	}
	if (groupStart != str.end() - 1)
		parts.push_back(current);
	AsnObjectIdentifier oid, src{ parts.size(), parts.data() };
	SnmpUtilOidCpy(&oid, &src);
	return oid;
}

std::wstring ReadStringValue(HKEY key, LPTSTR name){
	DWORD sz = 0, type = REG_SZ;
	LPBYTE tmp;

	if (RegQueryValueEx(key, name, 0, &type, NULL, &sz) != ERROR_SUCCESS || type != REG_SZ){
#if LOGGING
		logger << "::ReadStringValue cannot read value " << name << " length" << std::endl << std::flush;
#endif
		throw E_REG_ERROR;
	}

	tmp = new BYTE[sz + 2];
	if (RegQueryValueEx(key, name, 0, &type, tmp, &sz) != ERROR_SUCCESS){
#if LOGGING
		logger << "::ReadStringValue cannot read value " << name << std::endl << std::flush;
#endif
		throw E_REG_ERROR;
	}
	tmp[sz] = 0;
	tmp[sz + 1] = 0;
	std::wstring rv((TCHAR*)tmp);
	delete[]tmp;
	return rv;
}

Configuration RegistryConfiguration::ReadConfiguration(){
	Configuration rv;
	HKEY myRoot;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\SNMPCustom"), 0, KEY_READ | KEY_WOW64_64KEY, &myRoot) != ERROR_SUCCESS){
#if LOGGING
		logger << "RegistryConfiguration::ReadConfiguration cannot open root configuration key" << std::endl << std::flush;
#endif
		throw E_CANNOT_OPEN_KEY;
	}

	rv.Prefix = ParseId(ReadStringValue(myRoot, TEXT("Prefix")));

	DWORD index = 0;
	TCHAR keyName[256];
	DWORD sz = 255;
	while (RegEnumKeyEx(myRoot, index++, keyName, &sz, 0, NULL, NULL, NULL) == ERROR_SUCCESS){
		ConfigElement el;
		HKEY subkey;
		if (RegOpenKeyEx(myRoot, keyName, 0, KEY_READ | KEY_WOW64_64KEY, &subkey) != ERROR_SUCCESS)continue;
		sz = 0;
		el.Oid = ParseId(keyName);
		std::wstring tmp(ReadStringValue(subkey, TEXT("Backend")));
		transform(tmp.begin(), tmp.end(), tmp.begin(), toupper);
		if (tmp.compare(TEXT("WMI")) == 0)
			el.Backend = WMI;
		else if (tmp.compare(TEXT("PERFCOUNTER")) == 0)
			el.Backend = PerfCounter;
		else if (tmp.compare(TEXT("EXEC")) == 0){
			el.Backend = Exec;
			try{
				tmp = ReadStringValue(subkey, TEXT("Mode"));
			}
			catch (int){
				tmp = TEXT("");
			}
			transform(tmp.begin(), tmp.end(), tmp.begin(), toupper);
			if (tmp.compare(TEXT("RETVAL")) == 0)
				el.Mode = RetVal;
			else if (tmp.compare(TEXT("ERR")) == 0)
				el.Mode = StdErr;
			else
				el.Mode = StdOut;
		}
		else if (tmp.compare(TEXT("PGSQL")) == 0){
			el.Backend = PgSQL;
			el.ConnectionString = ReadStringValue(subkey, TEXT("ConnectionString"));
		}
		else{
#if LOGGING
			logger << "RegistryConfiguration::ReadConfiguration cannot parse backend for " << SnmpUtilOidToA(&el.Oid) << std::endl << std::flush;
#endif
			throw E_UNKNOWN_BACKEND;
		}

		tmp = ReadStringValue(subkey, TEXT("Type"));
		transform(tmp.begin(), tmp.end(), tmp.begin(), toupper);
		if (tmp.compare(TEXT("INT")) == 0){
			el.Type = ASN_INTEGER;
			if (el.Backend == PerfCounter){
				try{
					tmp = ReadStringValue(subkey, TEXT("Mode"));
				}
				catch (int){
					tmp = TEXT("");
				}
				transform(tmp.begin(), tmp.end(), tmp.begin(), toupper);
				if (tmp.compare(TEXT("AVERAGE")) == 0)
					el.Mode = Average;
				else if (tmp.compare(TEXT("MIN")) == 0)
					el.Mode = Min;
				else if (tmp.compare(TEXT("MAX")) == 0)
					el.Mode = Max;
				else
					el.Mode = Sample;
			}
		}
		else if (tmp.compare(TEXT("STRING")) == 0)
			el.Type = ASN_OCTETSTRING;
		else{
#if LOGGING
			logger << "RegistryConfiguration::ReadConfiguration cannot parse type for " << SnmpUtilOidToA(&el.Oid) << std::endl << std::flush;
#endif
			throw E_UNKNOWN_TYPE;
		}

		el.Param = ReadStringValue(subkey, TEXT("Param"));

		rv.Items.push_back(el);
		sz = 255;
	}
	return rv;
}