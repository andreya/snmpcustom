#include "stdafx.h"
#include "ExecBackend.h"

struct ProcInfo{
	PROCESS_INFORMATION pi;
	HANDLE hStdoutR;
	HANDLE hStdoutW;
	HANDLE hStderrR;
	HANDLE hStderrW;

	HANDLE hStdOutTmp;
	HANDLE hStdErrTmp;
};

ProcInfo ExecBackend::Execute(const ConfigElement &el){
	LPTSTR tstr = (LPTSTR)HeapAlloc(GetProcessHeap(), 0, (el.Param.size() + 1)*sizeof(TCHAR));
	tstr[0] = 0;
	StringCchCopy(tstr, el.Param.size() + 1, el.Param.c_str());
	STARTUPINFO si = { 0 };
	si.cb = sizeof(STARTUPINFO);
	SECURITY_ATTRIBUTES sa = { 0 };
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = NULL;
	PROCESS_INFORMATION pi = { 0 };	
	ProcInfo proc = { 0 };
	if (!CreatePipe(&proc.hStdOutTmp, &proc.hStdoutW, &sa, 0)){
#if LOGGING
		logger << "ExecBackend::Execute cannot create stdout pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}
	if (!DuplicateHandle(GetCurrentProcess(), proc.hStdOutTmp, GetCurrentProcess(), &proc.hStdoutR, 0,FALSE,DUPLICATE_SAME_ACCESS)){
#if LOGGING
		logger << "ExecBackend::Execute cannot duplicate stdout pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}
	/*if (!SetHandleInformation(proc.hStdoutR, HANDLE_FLAG_INHERIT, 0)){
#if LOGGING
		logger << "ExecBackend::Execute cannot update stdout pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}*/
	if (!CreatePipe(&proc.hStdErrTmp, &proc.hStderrW, &sa, 0)){
#if LOGGING
		logger << "ExecBackend::Execute cannot create stderr pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}
	if (!DuplicateHandle(GetCurrentProcess(), proc.hStdErrTmp, GetCurrentProcess(), &proc.hStderrR, 0, FALSE, DUPLICATE_SAME_ACCESS)){
#if LOGGING
		logger << "ExecBackend::Execute cannot duplicate stderr pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}
	/*if (!SetHandleInformation(proc.hStderrR, HANDLE_FLAG_INHERIT, 0)){
#if LOGGING
		logger << "ExecBackend::Execute cannot update stderr pipe" << std::endl << std::flush;
#endif
		throw E_CANNOT_INIT;
	}*/
	CloseHandle(proc.hStdOutTmp);
	CloseHandle(proc.hStdErrTmp);


	if (el.Mode != RetVal){
		si.dwFlags |= STARTF_USESTDHANDLES;
		si.hStdError = proc.hStderrW;
		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);;
		si.hStdOutput = proc.hStdoutW;
	}
	if (CreateProcess(NULL, tstr, NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS | /*CREATE_NO_WINDOW |*/ DETACHED_PROCESS, NULL, NULL, &si, &pi) != TRUE){
#if LOGGING
		logger << "ExecBackend::Execute cannot execute process: " << GetLastError() << std::endl << std::flush;
#endif
	}
	HeapFree(GetProcessHeap(), 0, tstr);	
	proc.pi = pi;
	CloseHandle(proc.hStdoutW);
	CloseHandle(proc.hStderrW);
	return proc; 
}

int ExecBackend::GetInt(const ConfigElement &el) {
	ProcInfo pi=Execute(el);
	int rv;
	if (el.Mode == RetVal){
		WaitForSingleObject(pi.pi.hProcess, INFINITE);
		DWORD code = 0;
		GetExitCodeProcess(pi.pi.hProcess, &code);
		rv = code;
	}
	else {
		HANDLE rh = el.Mode == StdErr ? pi.hStderrR : pi.hStdoutR;
		BYTE buf[4096];
		std::stringstream str;
		DWORD read;
		while (true){
			if (!ReadFile(rh, buf, sizeof(buf)-1, &read, NULL) && read == 0){
				break;
			}
			buf[read] = 0;
			str << buf;
		}
		rv = atoi(str.str().c_str());
		WaitForSingleObject(pi.pi.hProcess, INFINITE);
	}
	CloseHandle(pi.pi.hProcess);
	CloseHandle(pi.pi.hThread);
	return rv;
}
std::wstring ExecBackend::GetString(const ConfigElement &el) {
	ProcInfo pi = Execute(el);
	WaitForSingleObject(pi.pi.hProcess, INFINITE);
	std::wstring rv;
	if (el.Mode == RetVal){
		DWORD code = 0;
		GetExitCodeProcess(pi.pi.hProcess, &code);
		TCHAR buf[16];
		_itow_s(code, buf, 10);
		rv = buf;
	}
	else {
		HANDLE rh = el.Mode == StdErr ? pi.hStderrR : pi.hStdoutR;
		BYTE buf[4096];
		std::stringstream str;
		DWORD read;
		while (true){
			if (!ReadFile(rh, buf, sizeof(buf)-1, &read, NULL) && read == 0)break;
			buf[read] = 0;
			str << buf;
		}
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>conv;
		rv = conv.from_bytes(str.str());
		WaitForSingleObject(pi.pi.hProcess, INFINITE);
	}
	CloseHandle(pi.pi.hProcess);
	CloseHandle(pi.pi.hThread);
	return rv;
}