// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "ConfigBackend.h"
#include "ValueBackend.h"
#include <iostream>

#if LOGGING
std::ofstream logger;
#endif
std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
std::string cvt(const std::wstring &src){
	return converter.to_bytes(src);
}
std::wstring cvt(const std::string &src){
	return converter.from_bytes(src);
}

#if !TEST
BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
#if LOGGING
	TCHAR tmp[300];
	GetTempPath(300, tmp);
	lstrcat(tmp, TEXT("SMTPCustom.log"));
#endif
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
#if LOGGING
		logger = std::ofstream(tmp, std::ios_base::out);
		logger << "::DllMain" << std::endl << std::flush;
#endif
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#endif

#if TEST

extern Configuration gConfig;
extern std::vector<AbstractValueBackend*> gValueBackends;
BOOL SNMP_FUNC_TYPE SnmpExtensionInit(DWORD dwUptimeReference, HANDLE *phSubagentTrapEvent, AsnObjectIdentifier *pFirstSupportedRegion);

int main(){
#if LOGGING
	TCHAR tmp[300];
	GetTempPath(300, tmp);
	lstrcat(tmp, TEXT("SMTPCustom.log"));
	logger = std::ofstream(tmp, std::ios_base::out);
	logger << "::main" << std::endl << std::flush;
#endif
	AsnObjectIdentifier oid;
	SnmpExtensionInit(0, 0, &oid);
	for (std::vector<ConfigElement>::iterator it = gConfig.Items.begin(); it != gConfig.Items.end(); it++){
		std::wcout << it->OidString ;
		it->BackendData=gValueBackends[it->Backend]->Prepare(*it);
		for (int i = 0; i < 5; i++){
			Sleep(1000);
			std::wcout << ": <";
			if (it->Type == ASN_INTEGER)
				std::wcout << gValueBackends[it->Backend]->GetInt(*it);
			else
				std::wcout << gValueBackends[it->Backend]->GetString(*it);
			std::wcout << ">";
		}
		std::wcout << std::endl;
	}
	return 0;
}
int CALLBACK XWinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR args,int show){
	return main();
}
#endif