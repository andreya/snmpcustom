#include "stdafx.h"
#include "PerfCounterBackend.h"

class CounterData{
public:
	std::vector<int> values;
	HANDLE lock;
};

PerfCounterBackend::PerfCounterBackend() :
stopper(INVALID_HANDLE_VALUE),
collectionThread(INVALID_HANDLE_VALUE)
{
	PdhOpenQuery(NULL, NULL, &this->query);
}
PerfCounterBackend::~PerfCounterBackend(){
	ReleaseMutex(stopper);
	WaitForSingleObject(collectionThread, 2000);
	CloseHandle(stopper);
}
void* PerfCounterBackend::Prepare(const ConfigElement &el) {
	PDH_HCOUNTER counter;
	DWORD rv;
	if ((rv=PdhAddCounter(query, el.Param.c_str(), NULL, &counter)) != ERROR_SUCCESS){
#if LOGGING
		logger << "PerfCounterBackend::Prepare cannot add counter " << cvt(el.Param) << " error " << rv << std::endl << std::flush;
#endif
	}
	CounterData data;
	data.lock = CreateMutex(NULL, FALSE, NULL);
	counterData.insert(std::pair<PDH_HCOUNTER, CounterData>(counter, data));
	return counter;
}

DWORD WINAPI ThreadCallback(LPVOID param){
	return ((PerfCounterBackend*)param)->ThreadProc();
}
DWORD PerfCounterBackend::ThreadProc(){
#if LOGGING
	logger << "PerfCounterBackend::Prepare started fetcher thread" << std::endl << std::flush;
#endif
	PdhCollectQueryData(query);
	while (true){
		if (WaitForSingleObject(stopper, 1000) == WAIT_OBJECT_0)
			break;
		PdhCollectQueryData(query);
		for (CounterDataMap::iterator it = counterData.begin(); it != counterData.end(); it++){
			DWORD type;
			PDH_FMT_COUNTERVALUE xcounter;
			PdhGetFormattedCounterValue(it->first, PDH_FMT_LARGE, &type, &xcounter);
			if (xcounter.CStatus == 0){
				WaitForSingleObject(it->second.lock, INFINITE);
				it->second.values.push_back(xcounter.largeValue);
				ReleaseMutex(it->second.lock);
			}
		}
	}
	PdhCloseQuery(query);
#if LOGGING
	logger << "PerfCounterBackend::Prepare stopped fetcher thread" << std::endl << std::flush;
#endif
	return 0;
}
void PerfCounterBackend::Start(){
	stopper = CreateMutex(NULL, TRUE, NULL);
	collectionThread = CreateThread(NULL, 0, ThreadCallback, this, 0, 0);
}
int PerfCounterBackend::GetInt(const ConfigElement &el) {
	CounterDataMap::iterator item = counterData.find((PDH_HCOUNTER)el.BackendData);
	if (item == counterData.end()){
#if LOGGING
		logger << "PerfCounterBackend::GetInt cannot find counter " << cvt(el.Param) << std::endl << std::flush;
#endif
		return 0;
	}
	if (WaitForSingleObject(item->second.lock, 100) != WAIT_OBJECT_0){
#if LOGGING
		logger << "PerfCounterBackend::GetInt cannot acquire counter " << cvt(el.Param) << " lock" << std::endl << std::flush;
#endif
		return 0;
	}
	int rv;
	long tmp;
	std::vector<int>::iterator iter;
	switch (el.Mode){
	case Min:
		iter = std::min_element(item->second.values.begin(), item->second.values.end());
		if (iter != item->second.values.end())rv = *iter;
		else rv = 0;
		break;
	case Max:
		iter = std::max_element(item->second.values.begin(), item->second.values.end());
		if (iter != item->second.values.end())rv = *iter;
		else rv = 0;
		break;
	case Average:
		if (item->second.values.size() == 0)return 0;
		tmp = 0;
		for (iter = item->second.values.begin(); iter != item->second.values.end(); iter++){
			tmp += *iter;
		}
		rv = tmp / item->second.values.size();
		break;
	default:
		if (item->second.values.size() == 0)rv = 0;
		else rv = *item->second.values.begin();
	}
	item->second.values.clear();
	ReleaseMutex(item->second.lock);
	return rv;
}
std::wstring PerfCounterBackend::GetString(const ConfigElement &el) {
	int result = GetInt(el);
	TCHAR buf[16];
	_itow_s(result, buf, 10);
	return buf;
}
