#pragma once

#define E_CANNOT_OPEN_KEY 1
#define E_REG_ERROR 2
#define E_OID_ERROR 3
#define E_UNKNOWN_BACKEND 4
#define E_UNKNOWN_TYPE 5
#define E_CANNOT_INIT 6
#define E_CANNOT_GET 7

typedef enum {
	WMI,
	PerfCounter,
	Exec,
	PgSQL
} Backend;
typedef enum {
	Average=0,
	Min=1,
	Max=2,
	Sample=3,

	RetVal=0,
	StdOut=1,
	StdErr=2
} CollectionMode;

class ConfigElement{
public:
	std::wstring OidString;
	std::wstring Name;
	std::wstring Param;
	std::wstring ConnectionString;
	AsnObjectIdentifier Oid, FullOid;
	int Type;
	Backend Backend;
	CollectionMode Mode;
	void *BackendData;
	bool operator<(const ConfigElement &rhs) const { 
		int cmp = SnmpUtilOidCmp(const_cast<AsnObjectIdentifier*>(&(this->FullOid)), const_cast<AsnObjectIdentifier*>(&(rhs.FullOid)));
		return cmp<0; 
	}
};

typedef struct{
	std::vector<ConfigElement> Items;
	AsnObjectIdentifier Prefix;
} Configuration;

class AbstractConfiguration{
public:
	virtual Configuration ReadConfiguration() = 0;
};

class RegistryConfiguration :public AbstractConfiguration{
public:
	virtual Configuration ReadConfiguration();
};